//
//  main.m
//  Atlas
//
//  Created by Anton Davidsen on 26/03/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AtlasAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AtlasAppDelegate class]));
    }
}
