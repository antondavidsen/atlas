//
//  ArticleCell.m
//  Atlas
//
//  Created by Anton Davidsen on 25/07/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import "ArticleCell.h"
#import "Utility.h"

@implementation ArticleCell

@synthesize thumbnail = _thumbnail;
@synthesize link = _link;
@synthesize date = _date;
@synthesize title = _title;
@synthesize btn = _btn;
@synthesize activityIndicator = _activityIndicator;

#pragma mark - View Lifecycle

- (NSString *)reuseIdentifier {
    return @"ArticleCell";
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    self.thumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(20, 0, 208, 295)];
    self.thumbnail.opaque = YES;
    [self.contentView addSubview:self.thumbnail];
    
    self.btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.btn.frame = CGRectMake(20, 0, 20, 20);
    [self.btn setImage:[UIImage imageNamed:@"dot.png"] forState:UIControlStateNormal];
    self.btn.opaque = YES;
    self.btn.backgroundColor = greyColor;
    
    [self.btn addTarget:atlasViewController action:@selector(getIssueWithName:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.btn];
    
    self.title = [[UILabel alloc] initWithFrame:CGRectMake(20, 300, kCellWidth_iPad, 30)];
    self.title.opaque = YES;
    self.title.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
    self.title.backgroundColor = greyColor;
    [self.contentView addSubview:self.title];
    
    self.date = [[UILabel alloc] initWithFrame:CGRectMake(20, 330, kCellWidth_iPad, 30)];
    self.date.opaque = YES;
    self.date.font = [UIFont fontWithName:@"TrebuchetMS-Italic" size:12];
    self.date.backgroundColor = greyColor;
    [self.contentView addSubview:self.date];
    
    
    self.link = [[UILabel alloc] initWithFrame:CGRectMake(20, 360, kCellWidth_iPad, 30)];
    self.link.opaque = YES;
    self.link.backgroundColor = greyColor;
    self.link.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
    [self.contentView addSubview:self.link];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(20, 360, 195, 60)];
    self.activityIndicator.opaque = YES;
    self.activityIndicator.activityIndicatorViewStyle = UIActionSheetStyleBlackTranslucent;
    self.activityIndicator.backgroundColor = greyColor;
    [self.contentView addSubview:self.activityIndicator];
    
    self.backgroundColor = greyColor;
    self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.thumbnail.frame];
    self.selectedBackgroundView.backgroundColor = kHorizontalTableSelectedBackgroundColor;
    
    self.transform = CGAffineTransformMakeRotation(M_PI * 0.5);
    
    return self;
}

@end