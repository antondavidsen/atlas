//
//  ArticleViewController.h
//  Atlas
//
//  Created by Anton Davidsen on 21/04/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AtlasPopOverViewController.h"

@interface ArticleViewController : UIViewController<UIGestureRecognizerDelegate>{
    UIWebView *vw;
    NSURLRequest *indexArticle;
    AtlasPopOverViewController *atlasPopOverController;
    UIPopoverController *popOverController;
    UIBarButtonItem *shareBtn;
}

@property (nonatomic, strong) AtlasPopOverViewController *atlasPopOverController;
@property (nonatomic, strong) UIPopoverController *popOverController;
@property (strong, nonatomic) UIWebView *vw;
@property (strong, nonatomic) NSURLRequest *indexArticle;
@property (strong, nonatomic) UIBarButtonItem *shareBtn;
- (IBAction)onButtonClick;
+ (id)sharedManager;
@end
