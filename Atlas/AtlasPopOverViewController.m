//
//  AtlasPopOverViewController.m
//  Atlas
//
//  Created by Anton Davidsen on 12/06/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import "AtlasPopOverViewController.h"
#import "Social/Social.h"

@implementation AtlasPopOverViewController

#pragma mark -
#pragma mark Initialization

- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:style])) {
		self.preferredContentSize = CGSizeMake(200, 1 * 88 - 1);
    }
    return self;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
	self.tableView.rowHeight = 44.0;
	self.view.backgroundColor = [UIColor blackColor];
    
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 2;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell.
	NSString *string;
    if(indexPath.row == 0)
        string = @"Del på Twitter";
    if(indexPath.row == 1)
        string = @"Del på Facebook";
    
    cell.textLabel.text = string;
    
	cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SLComposeViewController *controller;
    
    if(indexPath.row == 0) {
        controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    }
    if(indexPath.row == 1) {
        controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    }
    
    
    // setup up social sharing
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter] && [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler = myBlock;
        
        [controller setInitialText:@"Læs den nye udgave af ATLAS på din iPad"];
        [controller addURL:[NSURL URLWithString:@"http://atlasmag.dk"]];
        [controller addImage:[UIImage imageNamed:@"atlas_newsstand.png"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Facebook / Twitter"
                                  message:@"For at dele indhold på Twitter eller Facebook skal du være logget ind"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

@end