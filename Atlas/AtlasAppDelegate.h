//
//  AtlasAppDelegate.h
//  Atlas
//
//  Created by Anton Davidsen on 26/03/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AtlasPublisher.h"
#import <NewRelicAgent/NewRelicAgent.h>


@interface AtlasAppDelegate : UIResponder <UIApplicationDelegate>{
    AtlasPublisher *publisher;
}

@property (strong, nonatomic) UIWindow *window;


@end
