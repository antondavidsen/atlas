//
//  ReceiptCheck.h
//  Atlas
//
//  Created by Anton Davidsen on 14/10/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReceiptCheck : NSObject {
    NSMutableData *receivedData;
}

+(ReceiptCheck *)validateReceiptWithData:(NSData *)receiptData completionHandler:(void(^)(BOOL,NSString *))handler;
+(ReceiptCheck *)validateSubscription:(NSString *)deviceToken completionHandler:(void(^)(BOOL,NSString *))handler;

+ (ReceiptCheck *) instance;
+(id)allocWithZone:(NSZone *)zone;

@property (nonatomic,copy) void(^completionBlock)(BOOL,NSString *);
@property (nonatomic,retain) NSData *receiptData;
@property (nonatomic,retain) NSString *deviceToken;

-(void)checkReceipt;
-(void)checkSubscription;

@end
