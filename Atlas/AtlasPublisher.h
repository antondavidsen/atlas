//
//  AtlasPublisher.h
//  Atlas
//
//  Created by Anton Davidsen on 26/03/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NewsstandKit/NewsstandKit.h>

extern  NSString *PublisherDidUpdateNotification;
extern  NSString *PublisherFailedUpdateNotification;

@interface AtlasPublisher : NSObject {
    NSArray *issues;
    
}

@property (nonatomic,readonly,getter = isReady) BOOL ready;
@property (strong, nonatomic) NSArray *issues;

-(void)addIssuesInNewsstand;
-(void)getIssuesList;
-(NSInteger)numberOfIssues;
-(NSString *)titleOfIssueAtIndex:(NSInteger)index;
-(NSString *)nameOfIssueAtIndex:(NSInteger)index;
-(NSString *)dateOfIssueAtIndex:(NSInteger)index;
-(NSDate *)getDateOfIssueAtIndex:(NSInteger)index;
-(NSString *)teaserOfIssueAtIndex:(NSInteger)index;
-(void)setCoverOfIssueAtIndex:(NSInteger)index completionBlock:(void(^)(UIImage *img))block;
-(NSURL *)contentURLforIssuesWithName:(NSString *)name;
-(NSString *)downloadPathForIssue:(NKIssue *)nkIssue;
-(NSString *)contentPathForIssue:(NKIssue *)nkIssue withNid:(NSString *)nid;
-(UIImage *)coverImageForIssue:(NKIssue *)nkIssue;
+ (AtlasPublisher *) instance;
+(id)allocWithZone:(NSZone *)zone;

@end
