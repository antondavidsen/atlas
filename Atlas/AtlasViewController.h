//
//  AtlasViewController.h
//  Atlas
//
//  Created by Anton Davidsen on 26/03/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NewsstandKit/NewsstandKit.h>
#import "AtlasPublisher.h"
#import "HorizontalTableCell.h"
#import <StoreKit/StoreKit.h>
#import "AtlasAppDelegate.h"


@interface AtlasViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,NSURLConnectionDownloadDelegate, NSURLConnectionDelegate, UIAlertViewDelegate, SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    AtlasPublisher *publisher;
    HorizontalTableCell *horizontalTableCell;
    AtlasAppDelegate *delegate;
    UIBarButtonItem *waitButton;
    UIBarButtonItem *refreshButton;
    UITableView *table;
    NSUInteger indexOfIssue;
    
}

@property (retain, nonatomic) IBOutlet UITableView *table;
@property (nonatomic) NSUInteger indexOfIssue;
@property (nonatomic,assign) BOOL purchasing;
@property (nonatomic,getter = isSubscribed, setter = setIsSubscribed:) BOOL subscribed;

-(IBAction)getIssueWithName:(id)sender;
-(void)trashContent;
-(void)loadIssues;
-(void)subscription;
-(void)readIssue:(NKIssue *)nkIssue;
-(void)downloadIssueAtIndex:(NSInteger)index;
+ (AtlasViewController *) instance;
+(id)allocWithZone:(NSZone *)zone;
-(BOOL)isSubscribed;

@end
