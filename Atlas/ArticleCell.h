//
//  ArticleCell.h
//  Atlas
//
//  Created by Anton Davidsen on 25/07/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AtlasViewController.h"

@interface ArticleCell : UITableViewCell {
    UIImageView *_thumbnail;
    UILabel *_link;
    UILabel *_date;
    UILabel *_title;
    UIButton *_btn;
    UIActivityIndicatorView *_activityIndicator;
    AtlasViewController *atlasViewController;
}

@property (nonatomic, strong) UIImageView *thumbnail;
@property (nonatomic, strong) UILabel *link;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *date;
@property (nonatomic, strong) UIButton *btn;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;


@end
