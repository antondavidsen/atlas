//
//  AtlasPublisher.m
//  Atlas
//
//  Created by Anton Davidsen on 26/03/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import "AtlasPublisher.h"
#import <NewsstandKit/NewsstandKit.h>
#import "Utility.h"

NSString *PublisherDidUpdateNotification = @"PublisherDidUpdate";
NSString *PublisherFailedUpdateNotification = @"PublisherFailedUpdate";

@interface AtlasPublisher ()

@end

@implementation AtlasPublisher

@synthesize ready;
@synthesize issues;


#pragma mark Singleton Methods

+ (AtlasPublisher *) instance {
    static dispatch_once_t _singletonPredicate;
    static AtlasPublisher *_singleton = nil;
    
    dispatch_once(&_singletonPredicate, ^{
        _singleton = [[super allocWithZone:nil] init];
    });
    
    return _singleton;
}

+ (id) allocWithZone:(NSZone *)zone {
    return [self instance];
}


-(id)init {
    
    self = [super init];
    if(self) {
        ready = NO;
        issues = nil;
    }
    return self;
}



-(void)getIssuesList {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),
                   ^{
                       NSString *path = [[NSString alloc] initWithFormat:@"%@/sites/default/files/ipad-issues/issues.plist", BASE_URL];
                       NSArray *tmpIssues = [NSArray arrayWithContentsOfURL:[NSURL URLWithString:path]];
                       
                       NSString *destPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                       destPath = [destPath stringByAppendingPathComponent:@"issues.plist"];
                       NSArray *localFile = [[NSArray alloc] initWithContentsOfFile:destPath];
                       
                       // If the file doesn't exist in the Documents Folder, copy it.
                       NSFileManager *fileManager = [NSFileManager defaultManager];
                       
                       if (![fileManager fileExistsAtPath:destPath]) {
                           NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"issues" ofType:@"plist"];
                           [fileManager copyItemAtPath:sourcePath toPath:destPath error:nil];
                       }
                       
                       // Overwrite plist from doc folder if new issues
                       if ([tmpIssues count] > [localFile count]) {
                           //overwrite file.
                           [tmpIssues writeToFile:destPath atomically:YES];
                       }
                       
                       
                       if(!tmpIssues) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [[NSNotificationCenter defaultCenter] postNotificationName:PublisherFailedUpdateNotification object:self];
                               // Load plist from documents folder
                               if([fileManager fileExistsAtPath:destPath]) {
                                   issues = [[NSArray alloc] initWithArray:localFile];
                                   
                               }
                               else {
                                   NSString *filePath = [[NSBundle mainBundle] pathForResource:@"issues" ofType:@"plist"];
                                   NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
                                   issues = [[NSArray alloc] initWithArray:[NSArray arrayWithArray:[dict objectForKey:@"Root"]]];
                                   NSLog(@"plist loaded from bundle");
                                   
                               }
                               ready = YES;
                               
                           });
                           
                       } else {
                           issues = [[NSArray alloc] initWithArray:tmpIssues];
                           
                           ready = YES;
                           [self addIssuesInNewsstand];
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [[NSNotificationCenter defaultCenter] postNotificationName:PublisherDidUpdateNotification object:self];
                               
                           });
                       }
                   });
    
}


// "issues" is the array of issues downloaded from the publisher's server
-(void)addIssuesInNewsstand {
    
    // shared NKLibrary instance
    NKLibrary *nkLib = [NKLibrary sharedLibrary];
    // we iterate through our issues and add only the one not in the NK library yet
    [issues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *name = [(NSDictionary *)obj objectForKey:@"Name"];
        NKIssue *nkIssue = [nkLib issueWithName:name];
        if(!nkIssue) {
            nkIssue = [nkLib addIssueWithName:name date:[(NSDictionary *)obj objectForKey:@"Date"]];
        }
        NSLog(@"Issue: %@",nkIssue);
    }];
    
}


-(NSInteger)numberOfIssues {
    if([self isReady] && issues){
        return [issues count];
    }
    else {
        return 0;
    }
}

-(NSDictionary *)issueAtIndex:(NSInteger)index{
    return [issues objectAtIndex:index];
}
-(NSString *)titleOfIssueAtIndex:(NSInteger)index{
    return [[self issueAtIndex:index] objectForKey:@"Title"];
}

-(NSString *)nameOfIssueAtIndex:(NSInteger)index{
    return [[self issueAtIndex:index] objectForKey:@"Name"];
}
-(NSString *)dateOfIssueAtIndex:(NSInteger)index{
    return [[self issueAtIndex:index] objectForKey:@"Date"];
}
-(NSDate *)getDateOfIssueAtIndex:(NSInteger)index{
    return [[self issueAtIndex:index] objectForKey:@"Date"];
}

-(NSString *)teaserOfIssueAtIndex:(NSInteger)index{
    return [[self issueAtIndex:index] objectForKey:@"Teaser"];
}

-(void)setCoverOfIssueAtIndex:(NSInteger)index completionBlock:(void(^)(UIImage *img))block {
    NSURL *coverURL = [NSURL URLWithString:[[self issueAtIndex:index] objectForKey:@"Cover"]];
    NSString *coverFileName = [coverURL lastPathComponent];
    NSString *coverFilePath = [CacheDirectory stringByAppendingPathComponent:coverFileName];
    UIImage *image = [UIImage imageWithContentsOfFile:coverFilePath];
    if(image) {
        block(image);
    } else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
                       ^{
                           NSData *imageData = [NSData dataWithContentsOfURL:coverURL];
                           UIImage *image = [UIImage imageWithData:imageData];
                           if(image) {
                               [imageData writeToFile:coverFilePath atomically:YES];
                               block(image);
                           }
                       });
    }
}

-(UIImage *)coverImageForIssue:(NKIssue *)nkIssue {
    NSString *name = nkIssue.name;
    
    for(NSDictionary *issueInfo in issues) {
        if([name isEqualToString:[issueInfo objectForKey:@"Name"]]) {
            NSString *coverPath = [issueInfo objectForKey:@"Cover"];
            NSString *coverName = [coverPath lastPathComponent];
            NSString *coverFilePath = [CacheDirectory stringByAppendingPathComponent:coverName];
            UIImage *image = [UIImage imageWithContentsOfFile:coverFilePath];
            return image;
        }
    }
    return nil;
}

-(NSURL *)contentURLforIssuesWithName:(NSString *)name {
    __block NSURL *contentURL=nil;
    [issues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *aName = [(NSDictionary *)obj objectForKey:@"Name"];
        if([aName isEqualToString:name]) {
            contentURL = [NSURL URLWithString:[(NSDictionary *)obj objectForKey:@"Content"]];
            *stop=YES;
        }
    }];
    NSLog(@"Content URL for issue with name %@ is %@",name,contentURL);
    return contentURL;
}

-(NSString *)downloadPathForIssue:(NKIssue *)nkIssue {
    return [[nkIssue.contentURL path] stringByAppendingPathComponent:@"issue.zip"];
}
-(NSString *)contentPathForIssue:(NKIssue *)nkIssue withNid:(NSString *)nid{
    NSString *string = [NSString stringWithFormat:@"issue.zip/index/index.html"];
    return [[nkIssue.contentURL path] stringByAppendingPathComponent:string];
}


@end
