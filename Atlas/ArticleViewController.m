//
//  ArticleViewController.m
//  Atlas
//
//  Created by Anton Davidsen on 21/04/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import "ArticleViewController.h"

@implementation ArticleViewController

@synthesize vw;
@synthesize indexArticle;
@synthesize popOverController;
@synthesize atlasPopOverController;
@synthesize shareBtn;
#pragma mark Singleton Methods


+ (id)sharedManager {
    static ArticleViewController *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {

    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
		self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

#pragma mark - View lifecycle

-(void)viewDidLoad 
{   [super viewDidLoad];
    
    // Add web view to viewcontroller, disable bounce
    UIWebView *myView = [[UIWebView alloc] init];
    vw = myView;
    [self setView:vw];

    self.title = @"ATLAS";
    [self setHidesBottomBarWhenPushed:YES]; // hides the tab bar
	[self.navigationController setToolbarHidden:NO animated:NO];
	self.navigationController.toolbar.barStyle = UIBarStyleBlack;
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    // Adds a share btn in navigation bar
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(onButtonClick)];
    shareBtn = btn;
    self.navigationItem.rightBarButtonItem = shareBtn;
    
    
    // Adds bar button items for the toolbar
    UIImage *toc = [UIImage imageNamed:@"toc.png"];
    UIImage *article = [UIImage imageNamed:@"article.png"];
    
    UIBarButtonItem *tocItem = [[UIBarButtonItem alloc] 
                                initWithImage:toc style:UIBarButtonItemStylePlain target:self action:@selector(goToArticle)];
    UIBarButtonItem *articleItem = [[UIBarButtonItem alloc] 
                                    initWithImage:article style:UIBarButtonItemStylePlain target:self action:@selector(goToOverview)];
    
    
    UIBarButtonItem *BtnSpace = [[UIBarButtonItem alloc ]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [self setToolbarItems:[NSArray arrayWithObjects:tocItem, articleItem, BtnSpace, nil]];
    
    
}

-(void)viewWillAppear:(BOOL)animated 
{   
    [super viewWillAppear:animated];
    [vw loadRequest:indexArticle];
}

- (void)viewWillDisappear:(BOOL)animated 
{
	[super viewWillDisappear:animated];
	[self.navigationController setToolbarHidden:YES animated:YES];
	self.navigationController.toolbar.barStyle = UIBarStyleBlack;
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)tapped:(UITapGestureRecognizer *)gesture {
    BOOL barsHidden = self.navigationController.navigationBar.hidden;
    [self.navigationController setToolbarHidden:!barsHidden animated:YES];
    [self.navigationController setNavigationBarHidden:!barsHidden animated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    }

-(void)goToArticle
{    
    // Go to front showing article overview
    [self.vw stringByEvaluatingJavaScriptFromString:@"getArticleOverview()"];
    
} 
-(void)goToOverview
{    
    // Go to front showing table of contents
    [self.vw stringByEvaluatingJavaScriptFromString:@"getArticleToc()"];

}


- (IBAction)onButtonClick{
    
    if (self.popOverController) {
        [self.popOverController dismissPopoverAnimated:YES];
		self.popOverController = nil;
	} else {

        
    AtlasPopOverViewController *pop = [[AtlasPopOverViewController alloc] init];
    
    UIPopoverController *poc = [[UIPopoverController alloc] initWithContentViewController:pop];
    [poc presentPopoverFromBarButtonItem:shareBtn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    self.popOverController = poc;
    }
	
}                           
@end
