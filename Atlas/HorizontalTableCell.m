//
//  HorizontalTableCell.m
//  Atlas
//
//  Created by Anton Davidsen on 25/07/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import "HorizontalTableCell.h"
#import "ArticleCell.h"
#import "Utility.h"
#import "AtlasViewController.h"
#import "AtlasPublisher.h"
#import "Reachability.h"


@implementation HorizontalTableCell

@synthesize horizontalTableView;

#pragma mark Singleton Methods

+ (HorizontalTableCell *) instance {
    static dispatch_once_t _singletonPredicate;
    static HorizontalTableCell *_singleton = nil;
    
    dispatch_once(&_singletonPredicate, ^{
        _singleton = [[super allocWithZone:nil] init];
    });
    
    return _singleton;
}

+ (id) allocWithZone:(NSZone *)zone {
    return [self instance];
}


#pragma mark - init methods


- (id)initWithFrame:(CGRect)frame
{
    
    if ((self = [super initWithFrame:frame])) {
        
        publisher = [AtlasPublisher instance];
        
        self.horizontalTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kCellHeight_iPad, kTableLength_iPad)];
        self.horizontalTableView.showsVerticalScrollIndicator = NO;
        self.horizontalTableView.showsHorizontalScrollIndicator = NO;
        self.horizontalTableView.transform = CGAffineTransformMakeRotation(-M_PI * 0.5);
        [self.horizontalTableView setFrame:CGRectMake(kRowHorizontalPadding_iPad * 0.5, kRowVerticalPadding_iPad * 0.5, kTableLength_iPad - kRowHorizontalPadding_iPad, kCellHeight_iPad)];
        
        self.horizontalTableView.rowHeight = kCellWidth_iPad;
        self.horizontalTableView.backgroundColor = kHorizontalTableBackgroundColor;
        
        self.horizontalTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.horizontalTableView.separatorColor = [UIColor clearColor];
        
        self.horizontalTableView.dataSource = self;
        self.horizontalTableView.delegate = self;
        [self addSubview:self.horizontalTableView];
    }
    
    return self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSUInteger index = indexPath.row;
    // Don't get first issue
    index = index+1;
    
    
    static NSString *CellIdentifier = @"ArticleCell";
    
    __block ArticleCell *cell = (ArticleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[ArticleCell alloc] initWithFrame:CGRectMake(0, 0, kCellWidth_iPad, kCellHeight_iPad)];
    }
    
    //Image
    cell.thumbnail.image = nil; // reset image as it will be retrieved asychronously
    [publisher setCoverOfIssueAtIndex:index completionBlock:^(UIImage *img) {
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.thumbnail.image=img;
        });
    }];
    
    cell.btn.tag = index;
    
    // Title
    cell.title.text = [[publisher titleOfIssueAtIndex:index] uppercaseString];
    
    //dates
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.YYYY"];
    NSString *theDate = [dateFormat stringFromDate:[publisher getDateOfIssueAtIndex:index]];
    NSString *date = [NSString stringWithFormat:@"Udgivet: %@", theDate];
    cell.date.text = date;
    
    // Link
    
    NKLibrary *nkLib = [NKLibrary sharedLibrary];
    NKIssue *nkIssue = [nkLib issueWithName:[publisher nameOfIssueAtIndex:index]];
    
    AtlasViewController *atlasViewController = [AtlasViewController instance];
    
   
    if(nkIssue.status==NKIssueContentStatusAvailable) {
        cell.link.text=@"LÆS ATLAS";
        cell.link.alpha=1.0;
        cell.btn.alpha = 1.0;
        [cell.activityIndicator stopAnimating];
        cell.activityIndicator.alpha=0.0;
    } else {
        if(nkIssue.status==NKIssueContentStatusDownloading) {
            [cell.activityIndicator startAnimating];
            cell.activityIndicator.alpha=1.0;
            cell.btn.alpha = 0.0;
            cell.link.alpha=0.0;
        } else {
            [cell.activityIndicator stopAnimating];
            cell.activityIndicator.alpha=0.0;
            cell.link.alpha=1.0;
            cell.btn.alpha = 0.0;
            cell.link.text=@"DOWNLOAD ATLAS";
        }
        
    }
    if (![atlasViewController isSubscribed]) {
        cell.link.text=@"ABONNÈR PÅ ATLAS";
    }

    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return ([publisher numberOfIssues] - 1);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // possible actions: read or download
    NKLibrary *nkLib = [NKLibrary sharedLibrary];
    
    NSUInteger index = indexPath.row;
    
    // Don't get first issue
    index = index+1;
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    NKIssue *nkIssue = [nkLib issueWithName:[publisher nameOfIssueAtIndex:index]];
    
    AtlasViewController *atlasViewController = [AtlasViewController instance];
    
    if ([atlasViewController isSubscribed]) {
        if(nkIssue.status==NKIssueContentStatusAvailable) {
            [atlasViewController readIssue:nkIssue];
        } else if(nkIssue.status==NKIssueContentStatusNone) {
            // Check for network status
            if (networkStatus == NotReachable) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ingen internetforbindelse" message:@"Tjek din internetforbindelse og prøv igen" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            } else {
                [atlasViewController downloadIssueAtIndex:index];
                [self.horizontalTableView reloadData];
                
            }
        }
    }
    else {
        // Check for network status
        if (networkStatus == NotReachable) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ingen internetforbindelse" message:@"Tjek din internetforbindelse og prøv igen" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        } else {

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abonnér på ATLAS"
                                                        message:@"Modtag månedens udgave af ATLAS på din iPad og få adgang til tidligere udgivelser af magasinet. Abonnér nu og få det hele gratis."
                                                       delegate:self
                                              cancelButtonTitle:@"Anuller"
                                              otherButtonTitles:@"Abonnér", nil];
        [alert setTag:1];
        [alert show];
        
        }
    }
}
#pragma mark alertview

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    AtlasViewController *atlasViewController = [AtlasViewController instance];
    
    if(alertView.tag == 1) {
        [atlasViewController subscription];
    }
}

@end