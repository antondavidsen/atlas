//
//  ReceiptCheck.m
//  Atlas
//
//  Created by Anton Davidsen on 14/10/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import "ReceiptCheck.h"
#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"
#import "AtlasAppDelegate.h"
#import "NSString+Base64.h"
#import "Utility.h"

@implementation ReceiptCheck

@synthesize receiptData;
@synthesize deviceToken;
@synthesize completionBlock;

+(ReceiptCheck *)validateReceiptWithData:(NSData *)_receiptData completionHandler:(void(^)(BOOL,NSString *))handler {
    ReceiptCheck *checker = [[ReceiptCheck alloc] init];
    checker.receiptData=_receiptData;
    checker.completionBlock=handler;
    [checker checkReceipt];
    return checker;
    
}
+(ReceiptCheck *)validateSubscription:(NSString *)_deviceToken completionHandler:(void (^)(BOOL, NSString *))handler {
    ReceiptCheck *subscription = [[ReceiptCheck alloc] init];
    subscription.deviceToken=_deviceToken;
    subscription.completionBlock=handler;
    [subscription checkSubscription];
    return subscription;
}

#pragma mark Singleton Methods

+ (ReceiptCheck *) instance {
    static dispatch_once_t _singletonPredicate;
    static ReceiptCheck *_singleton = nil;
    
    dispatch_once(&_singletonPredicate, ^{
        _singleton = [[super allocWithZone:nil] init];
    });
    
    return _singleton;
}

+ (id) allocWithZone:(NSZone *)zone {
    return [self instance];
}


-(void)checkReceipt {
    
    NSError *jsonError = nil;
    
    NSString *receiptBase64 = [NSString base64StringFromData:receiptData length:[receiptData length]];
    
    NSString *deviceTokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                receiptBase64,@"receipt_data",
                                                                SHARED_SECRET,@"password",
                                                                deviceTokenString, @"device_token",
                                                                @"free", @"subscription_type",
                                                                nil]
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&jsonError];
    
    NSDictionary *jsonObject=[NSJSONSerialization
                              JSONObjectWithData:jsonData
                              options:NSJSONReadingMutableLeaves
                              error:nil];

    
    // send encoded receipt 
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc]initWithBaseURL:url];
    [client setDefaultHeader:@"Accept" value:@"application/json"];
    [client setDefaultHeader:@"Content-Type" value:@"application/json"];
    [client setParameterEncoding:AFJSONParameterEncoding];
    
    NSString *path = [[NSString alloc] initWithFormat:@"%@/subscribe", API_URL];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST"
                                                            path:path
                                                  parameters:jsonObject];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [client registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Print the response body in text
        NSString *response = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        completionBlock(YES, response);
        
    
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Cannot transmit receipt data. %@",[error localizedDescription]);
        completionBlock(NO, @"Cannot create connection");
        NSLog(@"Error: %@", error);
    }];
    [operation start];

    NSLog(@"%@", operation);
}
-(void)checkSubscription {
    
    // check that a user is subscribed to an issue
    NSError *jsonError = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                deviceToken, @"device_token",
                                                                nil]
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&jsonError];
    
    NSDictionary *jsonObject=[NSJSONSerialization
                              JSONObjectWithData:jsonData
                              options:NSJSONReadingMutableLeaves
                              error:nil];
    
    
    // send encoded receipt
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc]initWithBaseURL:url];
    [client setDefaultHeader:@"Accept" value:@"application/json"];
    [client setDefaultHeader:@"Content-Type" value:@"application/json"];
    [client setParameterEncoding:AFJSONParameterEncoding];
    
    NSString *path = [[NSString alloc] initWithFormat:@"%@/validate", API_URL];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST"
                                                        path:path
                                                  parameters:jsonObject];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [client registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Print the response body in text
        NSString *response = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        completionBlock(YES, response);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Cannot transmit receipt data. %@",[error localizedDescription]);
        completionBlock(NO, @"Cannot create connection");
        NSLog(@"Error: %@", error);
    }];
    [operation start];
    
    NSLog(@"%@", operation);

}

@end
