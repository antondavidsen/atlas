//
//  AtlasAppDelegate.m
//  Atlas
//
//  Created by Anton Davidsen on 26/03/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import "AtlasAppDelegate.h"
#import "AtlasViewController.h"
#import <NewsstandKit/NewsstandKit.h>
#import "UAirship.h"
#import "UAPush.h"
#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"
#import "Utility.h"
#import <StoreKit/StoreKit.h>

@implementation AtlasAppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Add New Relic
    [NewRelicAgent startWithApplicationToken:@"AAe084f3165168babc43288631a75bf95737d556ab"];
    
    //Init Airship launch options
    NSMutableDictionary *takeOffOptions = [[NSMutableDictionary alloc] init];
    [takeOffOptions setValue:launchOptions forKey:UAirshipTakeOffOptionsLaunchOptionsKey];
    
    // Create Airship singleton that's used to talk to Urban Airship servers.
    // Please populate AirshipConfig.plist with your info from http://go.urbanairship.com
    [UAirship takeOff:takeOffOptions];
    
    
    // Register for notifications
    [[UAPush shared]
     registerForRemoteNotificationTypes:(UIRemoteNotificationTypeNewsstandContentAvailability |
                                         UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert)];

    
    // initialize atlasviewcontroller - required for all Newsstand functionality
    AtlasViewController *store = [AtlasViewController instance];
    
    // StoreKit transaction observer
    [[SKPaymentQueue defaultQueue] addTransactionObserver:store];
    
    // check if the application will run in background after being called by a push notification
    NSDictionary *payload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if(payload && [[payload objectForKey:@"aps"] objectForKey:@"content-available"]) {
    NSLog(@"Payload: %@", payload);
        // refresh the plist feed
        publisher = [[AtlasPublisher alloc] init];
        [publisher getIssuesList];
        
        // download new issue in the background, fetch info from payload
        NSString *issueName = [[payload objectForKey:@"aps"]objectForKey:@"issue-name"];
        NKIssue *newIssue = [[NKLibrary sharedLibrary] issueWithName:issueName];
        if(newIssue) {
            NSLog(@"%@", newIssue);
            NSURL *downloadURL = [NSURL URLWithString:[[payload objectForKey:@"aps"]objectForKey:@"download-url"]];
            NSURLRequest *req = [NSURLRequest requestWithURL:downloadURL];
            NKAssetDownload *assetDownload = [newIssue addAssetWithRequest:req];
            [assetDownload downloadWithDelegate:store];
            [store.table reloadData];
        }
        
    }

    // when the app is relaunched, it is better to restore pending downloading assets as abandoned downloadings will be cancelled
    NKLibrary *nkLib = [NKLibrary sharedLibrary];
    for(NKAssetDownload *asset in [nkLib downloadingAssets]) {
        NSLog(@"Asset to download: %@",asset);
        [asset downloadWithDelegate:store];            
    }
    
    // Override point for customization after application launch.
    return YES;
}
- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    // Check that receipt still validates
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [UAirship land];
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString *deviceTokenString = [[[[deviceToken description]
                                     stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                    stringByReplacingOccurrencesOfString: @">" withString: @""]
                                   stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"Registered with device token: %@",deviceTokenString);
    
    // Set device token
    [[NSUserDefaults standardUserDefaults] setObject:deviceTokenString forKey:@"deviceToken"];
    
    // Updates the device token and registers the token with UA
    [[UAPush shared] registerDeviceToken:deviceToken];
    
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failing in APNS registration: %@",error);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    UALOG(@"Received remote notification: %@", userInfo);
    
    
     [[UAPush shared] handleNotification:userInfo applicationState:application.applicationState];
     [[UAPush shared] resetBadge]; // zero badge after push received
    
    
    // Now check if it is new content; if so we show an alert
    if([[userInfo objectForKey:@"aps"]objectForKey:@"content-available"]) {
        if([[UIApplication sharedApplication] applicationState]==UIApplicationStateActive) {
            // active app -> display an alert

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ATLAS"
                                                            message:@"Der er udkommet en ny udgave af magasinet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
        } else {
            // download the new issue in background
            if(userInfo && [[userInfo objectForKey:@"aps"]objectForKey:@"content-available"]) {
                // refresh the plist feed
                publisher = [[AtlasPublisher alloc] init];
                [publisher getIssuesList];
                // download new issue in the background, fetch info from userInfo
                AtlasViewController *store = [AtlasViewController instance];
                [store.table reloadData];
                NSString *issueName = [[userInfo objectForKey:@"aps"]objectForKey:@"issue-name"];
                NKIssue *newIssue = [[NKLibrary sharedLibrary] issueWithName:issueName];
                if(newIssue) {
                    NSURL *downloadURL = [NSURL URLWithString:[[userInfo objectForKey:@"aps"]objectForKey:@"download-url"]];
                    NSURLRequest *req = [NSURLRequest requestWithURL:downloadURL];
                    NKAssetDownload *assetDownload = [newIssue addAssetWithRequest:req];
                    [assetDownload downloadWithDelegate:store];
                }
                
            }

        }
    }
}

@end
