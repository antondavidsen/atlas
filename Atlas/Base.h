//
//  Base.h
//  Atlas
//
//  Created by Anton Davidsen on 09/04/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Base : NSObject

-(NSString *)test;
@property (nonatomic, strong) IBOutlet UIImageView *image;
@property (nonatomic, strong) IBOutlet UILabel *label;

@end
