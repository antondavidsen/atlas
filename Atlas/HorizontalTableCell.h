//
//  HorizontalTableCell.h
//  Atlas
//
//  Created by Anton Davidsen on 25/07/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AtlasPublisher.h"

@interface HorizontalTableCell : UITableViewCell <UITableViewDelegate, UITableViewDataSource>
{
    UITableView *_horizontalTableView;
    AtlasPublisher *publisher;

}
@property (nonatomic, strong) UITableView *horizontalTableView;
+ (HorizontalTableCell *) instance;
+(id)allocWithZone:(NSZone *)zone;
@end