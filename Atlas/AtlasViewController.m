//
//  AtlasViewController.m
//  Atlas
//
//  Created by Anton Davidsen on 26/03/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//

#import "AtlasViewController.h"
#import "ArticleViewController.h"
#import "SSZipArchive.h"
#import "Utility.h"
#import "ReceiptCheck.h"
#import "Reachability.h"

@interface AtlasViewController (Private)

-(void)showIssues;
-(void)loadIssues;
-(void)readIssue:(NKIssue *)nkIssue;
-(void)downloadIssueAtIndex:(NSInteger)index;

@end

@implementation AtlasViewController
@synthesize table=table_;
@synthesize indexOfIssue;
@synthesize purchasing=purchasing_;
@synthesize subscribed;

#pragma mark Singleton Methods

+ (AtlasViewController *) instance {
    static dispatch_once_t _singletonPredicate;
    static AtlasViewController *_singleton = nil;
    
    dispatch_once(&_singletonPredicate, ^{
        _singleton = [[super allocWithZone:nil] init];
    });
    
    return _singleton;
}

+ (id) allocWithZone:(NSZone *)zone {
    return [self instance];
}
#pragma mark Init Methods

- (id)initWithCoder:(NSCoder*)aDecoder
{
    if(self = [super initWithCoder:aDecoder])
    {
        publisher = [AtlasPublisher instance];
        horizontalTableCell = [HorizontalTableCell instance];
        subscribed = NO;
    
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{   
    [super viewDidLoad];
    // define right bar button items
    refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadIssues)];
    UIActivityIndicatorView *loadingActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [loadingActivity startAnimating];
    
    waitButton = [[UIBarButtonItem alloc] initWithCustomView:loadingActivity];
    [waitButton setTarget:nil];
    [waitButton setAction:nil];
    
    
    // check if user is subscribed
    NSString *deviceTokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];

    [ReceiptCheck validateSubscription:deviceTokenString completionHandler:^(BOOL success,NSString *answer) {
        
        if(success==YES) {
            if ([answer isEqualToString:@"[true]"]) {
                [self setIsSubscribed:YES];
                [table_ reloadData];
                self.navigationItem.leftBarButtonItem = nil;
                [horizontalTableCell.horizontalTableView reloadData];
                
            }
            else {
                NSLog(@"Subscription is not valid: %@",answer);
                
            }
        } else {
            NSLog(@"An error occured: %@",answer);
        };
    }];
    

    
    //Add subscription option btn
    if (![self isSubscribed]) {
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"Abonnér" style:UIBarButtonItemStylePlain target:self action:@selector(chooseSubscription:)];
    }
    
    //Set bg color of table
    [table_ setBackgroundColor: greyColor];
    
    if([publisher isReady]) {
        [self showIssues];
       
    } else {
        [self loadIssues];
    }
    
}

- (void)viewDidUnload
{
    [self setTable:nil];
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);

}
#pragma mark - Publisher interaction

-(void)loadIssues {
    table_.alpha=0.0;
    horizontalTableCell.alpha=0.0;
    
    [self.navigationItem setRightBarButtonItem:waitButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(publisherReady:) name:PublisherDidUpdateNotification object:publisher];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(publisherFailed:) name:PublisherFailedUpdateNotification object:publisher];

    [publisher getIssuesList];    
    
}
 
-(void)publisherReady:(NSNotification *)not {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PublisherDidUpdateNotification object:publisher];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PublisherFailedUpdateNotification object:publisher];
    
    [self showIssues];
    
}

-(void)showIssues {
    [self.navigationItem setRightBarButtonItem:refreshButton];
    table_.alpha=1.0;
    horizontalTableCell.alpha=1.0;
    [horizontalTableCell.horizontalTableView reloadData];
    [table_ reloadData];
}

-(void)publisherFailed:(NSNotification *)not {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PublisherDidUpdateNotification object:publisher];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PublisherFailedUpdateNotification object:publisher];
   
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Offline"
                                                    message:@"Kan ikke oprette forbindelse til tjenesten. Fortsæt i offline-mode"
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert setTag:1];
    [alert show];

    [self.navigationItem setRightBarButtonItem:refreshButton];    
}


#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
 
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 0){
         return 130;
     }
    if(section == 1){
        return 30;
    }
    else{
        return false;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if(section == 0){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 700, 130)];
        label.text = @"ATLAS";
        label.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:80];
        label.backgroundColor = greyColor;
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, 700, 130)];  
        [headerView addSubview:label];
        return headerView;

    }
    if(section == 1){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 700, 30)];
        label.text = @"TIDLIGERE UDGIVELSER";
        label.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
        label.backgroundColor = greyColor;
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, 700, 30)];  
        [headerView addSubview:label];
        return headerView;

    }
    
    // TODO Add borderbottom
    return nil;
        
    }

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if(section == 0 || section == 1) {
         return 1;
    }
    else{
        return 0;
    }
        
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    NSUInteger index = 0;
    NSUInteger section = [indexPath section];
    UITableViewCell *cell = nil;
    
    // Large cell for current issue
    UITableViewCell *largeCell = [tableView dequeueReusableCellWithIdentifier:@"largeCell"];
    
    if (largeCell == nil) {
        largeCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier: @"largeCell"];
    }

    if(section == 0){
        cell = largeCell;
        index = indexPath.row;
    }
    // use horizontal cells for past issues
    if(section == 1){
        cell = horizontalTableCell;
    }
    
    cell.backgroundColor = greyColor;
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
    titleLabel.backgroundColor = greyColor;
    NSString *title = [publisher titleOfIssueAtIndex:index];
    titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
    titleLabel.text = [title uppercaseString];
 

    //dates
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:102];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.YYYY"];
    NSString *theDate = [dateFormat stringFromDate:[publisher getDateOfIssueAtIndex:index]];
    NSString *date = [NSString stringWithFormat:@"Udgivet: %@", theDate];
    dateLabel.text = date;
    
    //teaser
    UITextView *teaser = (UITextView *)[cell viewWithTag:99];
    teaser.text = [publisher teaserOfIssueAtIndex:index];
    teaser.backgroundColor = greyColor;
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    imageView.backgroundColor = greyColor;
    imageView.image=nil; // reset image as it will be retrieved asychronously
    [publisher setCoverOfIssueAtIndex:index completionBlock:^(UIImage *img) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
            imageView.image=img;
        });
    }];
    NKLibrary *nkLib = [NKLibrary sharedLibrary];
    NKIssue *nkIssue = [nkLib issueWithName:[publisher nameOfIssueAtIndex:index]];
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell viewWithTag:210];
    activityIndicator.activityIndicatorViewStyle = UIActionSheetStyleBlackTranslucent;
    
    UILabel *tapLabel = (UILabel *)[cell viewWithTag:104];
    tapLabel.backgroundColor = greyColor;
    if(nkIssue.status==NKIssueContentStatusAvailable) {
        tapLabel.text=@"LÆS ATLAS";
        tapLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
        tapLabel.alpha=1.0;
        [activityIndicator stopAnimating];
        activityIndicator.alpha=0.0;
    } else {
        if(nkIssue.status==NKIssueContentStatusDownloading) {
            [activityIndicator startAnimating];
            activityIndicator.alpha=1.0;
            tapLabel.alpha=0.0;
        } else {
            [activityIndicator stopAnimating];
            activityIndicator.alpha=0.0;
            tapLabel.alpha=1.0;
            tapLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
            tapLabel.text=@"DOWNLOAD ATLAS";
            
        }
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   
    NSUInteger section = indexPath.section;
    
    if(section == 0)
        return 350;
    else
        return 450;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // possible actions: read or download

    NKLibrary *nkLib = [NKLibrary sharedLibrary];
    
    NSUInteger section = indexPath.section;
    NSUInteger index = 0;
    
    
    if(section == 0)
        index = indexPath.row;
    if(section == 1)
        return;
    
    
    NKIssue *nkIssue = [nkLib issueWithName:[publisher nameOfIssueAtIndex:index]];
    // NSURL *downloadURL = [nkIssue contentURL];
    if(nkIssue.status==NKIssueContentStatusAvailable) {
        [self readIssue:nkIssue];
    } else if(nkIssue.status==NKIssueContentStatusNone) {
        // Check for network status
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ingen internetforbindelse" message:@"Tjek din internetforbindelse og prøv igen"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        } else {
            [self downloadIssueAtIndex:index];
            
        }
    }
}

#pragma mark - Issue actions

-(void)readIssue:(NKIssue *)nkIssue {
    [[NKLibrary sharedLibrary] setCurrentlyReadingIssue:nkIssue];
    ArticleViewController *articleViewController = [[ArticleViewController alloc] init];
    NSString *nid = [nkIssue name];
    
    NSURL *baseURL = [NSURL fileURLWithPath:[publisher contentPathForIssue:nkIssue withNid:nid]];
    NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
    articleViewController.indexArticle = request;
        
    // Fix navigation controller is not pushing
        
    [self.navigationController pushViewController:articleViewController animated:YES];

    // Remove badge when issue is read
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}
         

-(void)downloadIssueAtIndex:(NSInteger)index {
    
    NKLibrary *nkLib = [NKLibrary sharedLibrary];
    
    NKIssue *nkIssue = [nkLib issueWithName:[publisher nameOfIssueAtIndex:index]];
    NSURL *downloadURL = [publisher contentURLforIssuesWithName:nkIssue.name];
    
    if(!downloadURL) return;
    NSURLRequest *req = [NSURLRequest requestWithURL:downloadURL];
    NKAssetDownload *assetDownload = [nkIssue addAssetWithRequest:req];
    [assetDownload downloadWithDelegate:self];
    [assetDownload setUserInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithInt:index],@"Index",
                                nil]];
    
}

#pragma mark - NSURLConnectionDownloadDelegate

-(void)updateProgressOfConnection:(NSURLConnection *)connection withTotalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long)expectedTotalBytes {
    // get asset
    NKAssetDownload *dnl = connection.newsstandAssetDownload;
    UITableViewCell *cell = [table_ cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[[dnl.userInfo objectForKey:@"Index"] intValue] inSection:0]];
    
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell viewWithTag:211];
    activityIndicator.activityIndicatorViewStyle = UIActionSheetStyleBlackTranslucent;
    activityIndicator.alpha=1.0;
    [activityIndicator startAnimating];
    UIProgressView *progressView = (UIProgressView *)[cell viewWithTag:103];
    [[cell viewWithTag:210] setAlpha:1.0];
    progressView.progress=1.f*totalBytesWritten/expectedTotalBytes;   
}

-(void)connection:(NSURLConnection *)connection didWriteData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long)expectedTotalBytes {
    [self updateProgressOfConnection:connection withTotalBytesWritten:totalBytesWritten expectedTotalBytes:expectedTotalBytes];
}

-(void)connectionDidResumeDownloading:(NSURLConnection *)connection totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long)expectedTotalBytes {
    NSLog(@"Resume downloading %f",1.f*totalBytesWritten/expectedTotalBytes);
    [self updateProgressOfConnection:connection withTotalBytesWritten:totalBytesWritten expectedTotalBytes:expectedTotalBytes];    
}

-(void)connectionDidFinishDownloading:(NSURLConnection *)connection destinationURL:(NSURL *)destinationURL {
    // copy file to destination URL
    NKAssetDownload *dnl = connection.newsstandAssetDownload;
    NKIssue *nkIssue = dnl.issue;
    
    NSString *contentPath = [publisher downloadPathForIssue:nkIssue];
    NSError *deleteError=nil;
    
    // UNzip and move issue
    [SSZipArchive unzipFileAtPath:[destinationURL path] toDestination:contentPath];
    NSLog(@"File is being copied to %@", contentPath);
    
    //Delete tmp archive
    if([[NSFileManager defaultManager] removeItemAtPath:[destinationURL path] error:&deleteError]==NO) {
        NSLog(@"Error deleting zip-file: %@", destinationURL);
    }
    
    // Date of the latest issue
    NKLibrary *nkLibrary;
    NSDate *latestIssue = [publisher getDateOfIssueAtIndex:[nkLibrary.issues count]];
    
    // Update the Newsstand icon if there is a newer issue or for the first issue download
    if ([nkIssue.date compare:latestIssue] == NSOrderedDescending || [nkIssue.date compare:latestIssue] == NSOrderedSame) {
        UIImage *img = [publisher coverImageForIssue:nkIssue];
        if(img) {
            [[UIApplication sharedApplication] setNewsstandIconImage:img];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
        }
    }
    
    [table_ reloadData];
    [horizontalTableCell.horizontalTableView reloadData];
}

#pragma mark - Trash content

// remove all downloaded magazines
-(void)trashContent {
    NKLibrary *nkLib = [NKLibrary sharedLibrary];
    NSLog(@"%@",nkLib.issues);
    [nkLib.issues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [nkLib removeIssue:(NKIssue *)obj];
    }];
    [publisher addIssuesInNewsstand];
    [table_ reloadData];
}
- (IBAction)getIssueWithName:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    // Set index of issue that was clicked on
    self.indexOfIssue = button.tag;
  
    NSString *message = [NSString stringWithFormat:@"Ønsker du at slette %@?",  [publisher titleOfIssueAtIndex:button.tag]];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Slet magasin" message:message delegate:self cancelButtonTitle:@"Nej tak" otherButtonTitles:@"Ok", nil];
    [alert setTag:2];
    [alert show];
}
#pragma mark - alerts

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if(alertView.tag == 1){
        [self showIssues];
        [table_ reloadData];
    }
    if(alertView.tag == 2) {
        NKLibrary *nkLib = [NKLibrary sharedLibrary];
        NKIssue *nkIssue = [nkLib issueWithName:[publisher nameOfIssueAtIndex:indexOfIssue]];        
        [nkLib removeIssue:nkIssue];
        NSLog(@"Issue with name %@ was removed from the device", [publisher titleOfIssueAtIndex:indexOfIssue]);
        [publisher addIssuesInNewsstand];
        [table_ reloadData];
        [horizontalTableCell.horizontalTableView reloadData];
    }
    if(alertView.tag == 3) {
        [self subscription];
         NSLog(@"Free subscription choosen");
    }
}
    

#pragma mark StoreKit
- (IBAction)chooseSubscription:(id)sender {
    
    // Check for network status
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ingen internetforbindelse" message:@"Tjek din internetforbindelse og prøv igen"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        return;
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abonnér på ATLAS"
                                                       message:@"Modtag månedens udgave af ATLAS på din iPad og få adgang til tidligere udgivelser af magasinet. Abonnementet er gratis."
                                                      delegate:self
                                             cancelButtonTitle:@"Anuller"
                                             otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Abonnér"];
        [alert setTag:3];
        [alert show];

        
    }
}


- (void)subscription {
        
    // Hard code free subscription
    NSString *productId = @"dk.atlasmag.newsstand.free";
    
    if(purchasing_==YES) {
        return;
    }
    purchasing_=YES;
    // product request
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:productId]];
    productsRequest.delegate=self;
    [productsRequest start];
}
-(void)requestDidFinish:(SKRequest *)request {
    purchasing_=NO;
    NSLog(@"Request: %@",request);
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    purchasing_=NO;
    NSLog(@"Request %@ failed with error %@",request,error);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Der er sket en fejl" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Annuller" otherButtonTitles:nil];
    [alert show];
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"Request: %@ -- Response: %@",request,response);
    NSLog(@"Products: %@",response.products);
    for(SKProduct *product in response.products) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
}
-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for(SKPaymentTransaction *transaction in transactions) {
        NSLog(@"Updated transaction %@",transaction);
        switch (transaction.transactionState) {
            case SKPaymentTransactionStateFailed:
                [self errorWithTransaction:transaction];
                break;
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing...");
                break;
            case SKPaymentTransactionStatePurchased:
            case SKPaymentTransactionStateRestored:
                [self finishedTransaction:transaction];
                break;
            default:
                break;
        }
    }
}
-(void)finishedTransaction:(SKPaymentTransaction *)transaction {
    
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    NSData *receipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    
    [self checkReceipt:receipt];
}

-(void)errorWithTransaction:(SKPaymentTransaction *)transaction {
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Der er opstået en fejl. Abonnementet kunne ikke gennemføres."
                                                    message:[transaction.error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)checkReceipt:(NSData *)receipt {
        
    [ReceiptCheck validateReceiptWithData:receipt completionHandler:^(BOOL success,NSString *answer){
        if(success==YES && ![answer isEqualToString:@"[false]"]) {
            NSLog(@"Receipt has been validated: %@",answer);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Du abonnerer nu på ATLAS" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            subscribed = YES;
            [table_ reloadData];
            self.navigationItem.leftBarButtonItem = nil;
            [horizontalTableCell.horizontalTableView reloadData];
        
        } else {
            NSLog(@"Receipt not validated! Error: %@",answer);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abonnementet kunne ikke gennemføres" message:@"Data fra iTunes kunne ikke valideres" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        };
    }];
}
@end