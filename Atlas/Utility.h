//
//  Utility.h
//  Atlas
//
//  Created by Anton Davidsen on 25/07/12.
//  Copyright (c) 2012 Anywear. All rights reserved.
//


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// colors
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define SHARED_SECRET @"2ebfff703a054300a0da794bcb499eca"
#define API_URL @"http://atlasmag.dk/api"
#define BASE_URL @"http://atlasmag.dk"

//#define API_URL @"http://atlasmag.localhost/api"
//#define BASE_URL @"http://atlasmag.localhost"


# define greyColor                                      [UIColor colorWithRed:0.97254901960784 green:0.96470588235294 blue:0.95686274509804 alpha:1]
# define tapColor                                       [UIColor colorWithRed:0.95686274509804 green:0.95686274509804 blue:0.95686274509804 alpha:1]


// The background color of the vertical table view
#define kVerticalTableBackgroundColor               greyColor
// Background color for the horizontal table view (the one embedded inside the rows of our vertical table)
#define kHorizontalTableBackgroundColor             greyColor

// The background color on the horizontal table view for when we select a particular cell
#define kHorizontalTableSelectedBackgroundColor     tapColor

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// sizes
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// Width (or length before rotation) of the table view embedded within another table view's row
#define kTableLength_iPad                               768

// Height for the Headlines section of the main (vertical) table view
#define kHeadlinesSectionHeight_iPad                    130

// Height for regular sections in the main table view
#define kRegularSectionHeight_iPad                      130

// Width of the cells of the embedded table view (after rotation, which means it controls the rowHeight property)
#define kCellWidth_iPad                                 260
// Height of the cells of the embedded table view (after rotation, which would be the table's width)
#define kCellHeight_iPad                                450

// Padding for the Cell containing the article image and title
#define kArticleCellVerticalInnerPadding_iPad           0
#define kArticleCellHorizontalInnerPadding_iPad         0

// Vertical padding for the embedded table view within the row
#define kRowVerticalPadding_iPad                        20
// Horizontal padding for the embedded table view within the row
#define kRowHorizontalPadding_iPad                      0
